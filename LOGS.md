LOGS

Beginning - till `git annex uninit`

```bash
➜  git-annex git:(master) ✗ git checkout -b push-before-annex
Switched to a new branch 'push-before-annex'
➜  git-annex git:(push-before-annex) ✗ git status
On branch push-before-annex
Untracked files:
  (use "git add <file>..." to include in what will be committed)

	.gitignore
	guide.md
	images/

nothing added to commit but untracked files present (use "git add" to track)
➜  git-annex git:(push-before-annex) ✗ git add .
➜  git-annex git:(push-before-annex) ✗ git commit -m "push files to feature-branch"
[push-before-annex 429d57d] push files to feature-branch
 4 files changed, 154 insertions(+)
 create mode 100644 .gitignore
 create mode 100644 guide.md
 create mode 100644 images/01.png
 create mode 100644 images/02.png
➜  git-annex git:(push-before-annex) git push origin push-before-annex
Counting objects: 7, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (6/6), done.
Writing objects: 100% (7/7), 79.16 KiB | 0 bytes/s, done.
Total 7 (delta 0), reused 0 (delta 0)
remote:
remote: To create a merge request for push-before-annex, visit:
remote:   https://gitlab.com/gitlab-tests/git-annex/merge_requests/new?merge_request%5Bsource_branch%5D=push-before-annex
remote:
To gitlab.com:gitlab-tests/git-annex.git
 * [new branch]      push-before-annex -> push-before-annex
➜  git-annex git:(push-before-annex) git checkout master
Switched to branch 'master'
Your branch is up-to-date with 'origin/master'.
➜  git-annex git:(master) ✗ git pull origin master
remote: Counting objects: 1, done.
remote: Total 1 (delta 0), reused 0 (delta 0)
Unpacking objects: 100% (1/1), done.
From gitlab.com:gitlab-tests/git-annex
 * branch            master     -> FETCH_HEAD
   580b1ee..b9e17ed  master     -> origin/master
Updating 580b1ee..b9e17ed
Fast-forward
 .gitignore    |   1 +
 guide.md      | 153 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 images/01.png | Bin 0 -> 14203 bytes
 images/02.png | Bin 0 -> 75200 bytes
 4 files changed, 154 insertions(+)
 create mode 100644 .gitignore
 create mode 100644 guide.md
 create mode 100644 images/01.png
 create mode 100644 images/02.png
➜  git-annex git:(master) git annex init 'test'
init test ok
(recording state in git...)
➜  git-annex git:(master) git status
On branch master
Your branch is up-to-date with 'origin/master'.
nothing to commit, working tree clean
➜  git-annex git:(master) git annex info
repository mode: indirect
trusted repositories: 0
semitrusted repositories: 4
	00000000-0000-0000-0000-000000000001 -- web
 	00000000-0000-0000-0000-000000000002 -- bittorrent
 	12402f15-7028-4904-aacf-1f38b6ddf924 -- origin
 	c6c64fca-4f02-4680-9f6f-2e28b3aceca7 -- test [here]
untrusted repositories: 0
transfers in progress: none
available local disk space: 147.86 gigabytes (+1 megabyte reserved)
local annex keys: 0
local annex size: 0 bytes
annexed files in working tree: 0
size of annexed files in working tree: 0 bytes
bloom filter size: 32 mebibytes (0% full)
backend usage:
➜  git-annex git:(master) git status
On branch master
Your branch is up-to-date with 'origin/master'.
nothing to commit, working tree clean
➜  git-annex git:(master) git annex add images/*
➜  git-annex git:(master) git annex info
repository mode: indirect
trusted repositories: 0
semitrusted repositories: 4
	00000000-0000-0000-0000-000000000001 -- web
 	00000000-0000-0000-0000-000000000002 -- bittorrent
 	12402f15-7028-4904-aacf-1f38b6ddf924 -- origin
 	c6c64fca-4f02-4680-9f6f-2e28b3aceca7 -- test [here]
untrusted repositories: 0
transfers in progress: none
available local disk space: 147.86 gigabytes (+1 megabyte reserved)
local annex keys: 0
local annex size: 0 bytes
annexed files in working tree: 0
size of annexed files in working tree: 0 bytes
bloom filter size: 32 mebibytes (0% full)
backend usage:
➜  git-annex git:(master) git annex whereis images/01.png
➜  git-annex git:(master) git annex sync --content
commit
On branch master
Your branch is up-to-date with 'origin/master'.
nothing to commit, working tree clean
ok
pull origin
remote: Counting objects: 2, done.
remote: Total 2 (delta 0), reused 0 (delta 0)
Unpacking objects: 100% (2/2), done.
From gitlab.com:gitlab-tests/git-annex
 * [new branch]      git-annex  -> origin/git-annex
ok
(merging origin/git-annex into git-annex...)
(recording state in git...)
push origin
Counting objects: 5, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (4/4), done.
Writing objects: 100% (5/5), 575 bytes | 0 bytes/s, done.
Total 5 (delta 1), reused 0 (delta 0)
remote:
remote: To create a merge request for synced/git-annex, visit:
remote:   https://gitlab.com/gitlab-tests/git-annex/merge_requests/new?merge_request%5Bsource_branch%5D=synced%2Fgit-annex
remote:
remote: To create a merge request for synced/master, visit:
remote:   https://gitlab.com/gitlab-tests/git-annex/merge_requests/new?merge_request%5Bsource_branch%5D=synced%2Fmaster
remote:
To gitlab.com:gitlab-tests/git-annex.git
 * [new branch]      git-annex -> synced/git-annex
 * [new branch]      master -> synced/master
ok
➜  git-annex git:(master) git annex whereis images/01.png
➜  git-annex git:(master) git annex info
repository mode: indirect
trusted repositories: 0
semitrusted repositories: 4
	00000000-0000-0000-0000-000000000001 -- web
 	00000000-0000-0000-0000-000000000002 -- bittorrent
 	12402f15-7028-4904-aacf-1f38b6ddf924 -- origin
 	c6c64fca-4f02-4680-9f6f-2e28b3aceca7 -- test [here]
untrusted repositories: 0
transfers in progress: none
available local disk space: 147.86 gigabytes (+1 megabyte reserved)
local annex keys: 0
local annex size: 0 bytes
annexed files in working tree: 0
size of annexed files in working tree: 0 bytes
bloom filter size: 32 mebibytes (0% full)
backend usage:
➜  git-annex git:(master) git status
On branch master
Your branch is up-to-date with 'origin/master'.
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   images/01.png

no changes added to commit (use "git add" and/or "git commit -a")
➜  git-annex git:(master) ✗ git annex add images/*
add images/01.png ok
(recording state in git...)
➜  git-annex git:(master) ✗ git annex whereis images/01.png
whereis images/01.png (1 copy)
  	c6c64fca-4f02-4680-9f6f-2e28b3aceca7 -- test [here]
ok
➜  git-annex git:(master) ✗ git annex add .
➜  git-annex git:(master) ✗ git commit -m "modify img 01"
[master c55b920] modify img 01
 1 file changed, 0 insertions(+), 0 deletions(-)
 rewrite images/01.png (100%)
 mode change 100644 => 120000
➜  git-annex git:(master) git annex sync --content
commit
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)
nothing to commit, working tree clean
ok
pull origin
ok
copy images/01.png (checking origin...) (to origin...)
SHA256E-s5513--9526d65c8254a5a5de98bd236bd55e635b1433974756b76dfa4224cd0a3e1785.png
        5513 100%    0.00kB/s    0:00:00 (xfer#1, to-check=0/1)

sent 5677 bytes  received 42 bytes  3812.67 bytes/sec
total size is 5513  speedup is 0.96
ok
pull origin
ok
(recording state in git...)
push origin
Counting objects: 14, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (12/12), done.
Writing objects: 100% (14/14), 1.27 KiB | 0 bytes/s, done.
Total 14 (delta 2), reused 0 (delta 0)
remote:
remote: To create a merge request for synced/git-annex, visit:
remote:   https://gitlab.com/gitlab-tests/git-annex/merge_requests/new?merge_request%5Bsource_branch%5D=synced%2Fgit-annex
remote:
remote: To create a merge request for synced/master, visit:
remote:   https://gitlab.com/gitlab-tests/git-annex/merge_requests/new?merge_request%5Bsource_branch%5D=synced%2Fmaster
remote:
To gitlab.com:gitlab-tests/git-annex.git
   6ce86b0..cb4e938  git-annex -> synced/git-annex
   b9e17ed..c55b920  master -> synced/master
ok
➜  git-annex git:(master) git status
On branch master
Your branch is up-to-date with 'origin/master'.
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   images/02.png

no changes added to commit (use "git add" and/or "git commit -a")
➜  git-annex git:(master) ✗ git annex add .
add images/02.png ok
(recording state in git...)
➜  git-annex git:(master) ✗ git commit -m "modify img 02"
[master 943444b] modify img 02
 1 file changed, 0 insertions(+), 0 deletions(-)
 rewrite images/02.png (100%)
 mode change 100644 => 120000
➜  git-annex git:(master) git annex sync --content
commit
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)
nothing to commit, working tree clean
ok
pull origin
remote: Counting objects: 5, done.
remote: Compressing objects: 100% (4/4), done.
remote: Total 5 (delta 2), reused 0 (delta 0)
Unpacking objects: 100% (5/5), done.
From gitlab.com:gitlab-tests/git-annex
   cb4e938..02e119b  git-annex  -> origin/git-annex
ok
(merging origin/git-annex into git-annex...)
(recording state in git...)
copy images/02.png (checking origin...) (to origin...)
SHA256E-s73354--e191a8ea3f4ce5c0cecca647473cb797f6ca84d75dea50c4cddc7a187a8da39a.png
       73354 100%   38.71MB/s    0:00:00 (xfer#1, to-check=0/1)

sent 73527 bytes  received 42 bytes  16348.67 bytes/sec
total size is 73354  speedup is 1.00
ok
pull origin
ok
(recording state in git...)
push origin
Counting objects: 16, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (14/14), done.
Writing objects: 100% (16/16), 1.62 KiB | 0 bytes/s, done.
Total 16 (delta 2), reused 0 (delta 0)
remote:
remote: To create a merge request for synced/git-annex, visit:
remote:   https://gitlab.com/gitlab-tests/git-annex/merge_requests/new?merge_request%5Bsource_branch%5D=synced%2Fgit-annex
remote:
remote: To create a merge request for synced/master, visit:
remote:   https://gitlab.com/gitlab-tests/git-annex/merge_requests/new?merge_request%5Bsource_branch%5D=synced%2Fmaster
remote:
To gitlab.com:gitlab-tests/git-annex.git
   cb4e938..186d141  git-annex -> synced/git-annex
   c55b920..943444b  master -> synced/master
ok
➜  git-annex git:(master) git annex direct
commit
On branch master
Your branch is up-to-date with 'origin/master'.
nothing to commit, working tree clean
ok
direct images/01.png ok
direct images/02.png ok
direct  ok
➜  git-annex git:(annex/direct/master) git status
fatal: This operation must be run in a work tree
➜  git-annex git:(annex/direct/master) git annex uninit
unannex images/01.png ok
unannex images/02.png ok
Deleted branch git-annex (was 186d141).
➜  git-annex git:(annex/direct/master) git status
fatal: This operation must be run in a work tree
➜  git-annex git:(annex/direct/master) git annex sync --content
(merging origin/git-annex into git-annex...)
(recording state in git...)
commit  (recording state in git...)
(recording state in git...)
ok
pull origin
remote: Counting objects: 5, done.
remote: Compressing objects: 100% (4/4), done.
remote: Total 5 (delta 2), reused 0 (delta 0)
Unpacking objects: 100% (5/5), done.
From gitlab.com:gitlab-tests/git-annex
   186d141..ae4d96e  git-annex  -> origin/git-annex
ok
(merging origin/git-annex into git-annex...)
(recording state in git...)
fatal: Cannot force update the current branch.
git-annex: failed to update refs/heads/master
➜  git-annex git:(master) git add .
fatal: This operation must be run in a work tree
➜  git-annex git:(master) git status
fatal: This operation must be run in a work tree
➜  git-annex git:(master) git annex indirect
commit  (recording state in git...)

ok
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)
Untracked files:
	images/

nothing added to commit but untracked files present
ok
indirect  ok
ok
➜  git-annex git:(master) ✗ git status
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)
Untracked files:
  (use "git add <file>..." to include in what will be committed)

	images/

nothing added to commit but untracked files present (use "git add" to track)
➜  git-annex git:(master) ✗ git add .
➜  git-annex git:(master) ✗ git commit -m "uninit"
[master 1cd371e] uninit
 2 files changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 images/01.png
 create mode 100644 images/02.png
➜  git-annex git:(master) git push origin master
Counting objects: 7, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (7/7), done.
Writing objects: 100% (7/7), 66.74 KiB | 0 bytes/s, done.
Total 7 (delta 2), reused 0 (delta 0)
To gitlab.com:gitlab-tests/git-annex.git
   943444b..1cd371e  master -> master
➜  git-annex git:(master)
```

Modification to img 02 after uninit:

```bash
➜  git-annex git:(master) git status
On branch master
Your branch is up-to-date with 'origin/master'.
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   guide.md

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	LOGS.md

no changes added to commit (use "git add" and/or "git commit -a")
➜  git-annex git:(master) ✗ git add .
➜  git-annex git:(master) ✗ git commit -m "add logs, update guide"
[master c91b204] add logs, update guide
 2 files changed, 437 insertions(+), 20 deletions(-)
 create mode 100644 LOGS.md
➜  git-annex git:(master) git push origin master
Counting objects: 4, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (4/4), done.
Writing objects: 100% (4/4), 3.78 KiB | 0 bytes/s, done.
Total 4 (delta 1), reused 0 (delta 0)
To gitlab.com:gitlab-tests/git-annex.git
   1cd371e..c91b204  master -> master
➜  git-annex git:(master) git status
On branch master
Your branch is up-to-date with 'origin/master'.
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   images/02.png

no changes added to commit (use "git add" and/or "git commit -a")
➜  git-annex git:(master) ✗ git add .
➜  git-annex git:(master) ✗ git commit -m "modify img 02 again"
[master 39ecc5a] modify img 02 again
 1 file changed, 0 insertions(+), 0 deletions(-)
➜  git-annex git:(master) git push origin master
Counting objects: 4, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (4/4), done.
Writing objects: 100% (4/4), 9.57 KiB | 0 bytes/s, done.
Total 4 (delta 2), reused 0 (delta 0)
To gitlab.com:gitlab-tests/git-annex.git
   c91b204..39ecc5a  master -> master
```

Check Git LFS

```bash
➜  git-annex git:(master) git lfs help
git lfs <command> [<args>]

Git LFS is a system for managing and versioning large files in
association with a Git repository.  Instead of storing the large files
within the Git repository as blobs, Git LFS stores special "pointer
files" in the repository, while storing the actual file contents on a
Git LFS server.  The contents of the large file are downloaded
automatically when needed, for example when a Git branch containing
the large file is checked out.

Git LFS works by using a "smudge" filter to look up the large file
contents based on the pointer file, and a "clean" filter to create a
new version of the pointer file when the large file's contents change.
It also uses a pre-push hook to upload the large file contents to
the Git LFS server whenever a commit containing a new large file
version is about to be pushed to the corresponding Git server.

Commands
--------

Like Git, Git LFS commands are separated into high level ("porcelain")
commands and low level ("plumbing") commands.

High level commands
--------------------

* git lfs env:
    Display the Git LFS environment.
* git lfs checkout:
    Populate working copy with real content from Git LFS files
* git lfs fetch:
    Download git LFS files from a remote
* git lfs fsck:
    Check GIT LFS files for consistency.
* git lfs init:
    Ensure Git LFS is configured properly.
* git lfs logs:
    Show errors from the git-lfs command.
* git lfs ls-files:
    Show information about Git LFS files in the index and working tree.
* git lfs pull:
    Fetch LFS changes from the remote & checkout any required working tree files
* git lfs push:
    Push queued large files to the Git LFS endpoint.
* git lfs status:
    Show the status of Git LFS files in the working tree.
* git lfs track:
    View or add Git LFS paths to Git attributes.
* git lfs untrack:
    Remove Git LFS paths from Git Attributes.
* git lfs update:
    Update Git hooks for the current Git repository.

Low level commands
-------------------

* git lfs clean:
    Git clean filter that converts large files to pointers.
* git lfs pointer:
    Build and compare pointers.
* git lfs pre-push:
    Git pre-push hook implementation.
* git lfs smudge:
    Git smudge filter that converts pointer in blobs to the actual content.
➜  git-annex git:(master) git status
On branch master
Your branch is up-to-date with 'origin/master'.
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   guide.md

no changes added to commit (use "git add" and/or "git commit -a")
➜  git-annex git:(master) ✗ git add .
➜  git-annex git:(master) ✗ git commit -m "update guide - and test the response in the remote by deleting annex-related branches"
[master ac9da0e] update guide - and test the response in the remote by deleting annex-related branches
 1 file changed, 15 insertions(+)
➜  git-annex git:(master) git push origin master
Counting objects: 3, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 638 bytes | 0 bytes/s, done.
Total 3 (delta 2), reused 0 (delta 0)
To gitlab.com:gitlab-tests/git-annex.git
   39ecc5a..ac9da0e  master -> master
➜  git-annex git:(master) git pull origin
Already up-to-date.
➜  git-annex git:(master) git status
On branch master
Your branch is up-to-date with 'origin/master'.
nothing to commit, working tree clean
```

Git LFS track files:

```
➜  git-annex git:(master) git lfs track images/*
Tracking images/01.png
Tracking images/02.png
➜  git-annex git:(master) ✗ git status
On branch master
Your branch is up-to-date with 'origin/master'.
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   guide.md

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	.gitattributes

no changes added to commit (use "git add" and/or "git commit -a")
➜  git-annex git:(master) ✗ git add .
➜  git-annex git:(master) ✗ git commit -m "push .gitattributes created by LFS, update guide"
(recording state in git...)
[master 9994265] push .gitattributes created by LFS, update guide
 2 files changed, 41 insertions(+), 1 deletion(-)
 create mode 100644 .gitattributes
➜  git-annex git:(master) git push origin master
Counting objects: 4, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (4/4), done.
Writing objects: 100% (4/4), 1.06 KiB | 0 bytes/s, done.
Total 4 (delta 1), reused 0 (delta 0)
To gitlab.com:gitlab-tests/git-annex.git
   ac9da0e..9994265  master -> master
➜  git-annex git:(master) git lfs ls-files
➜  git-annex git:(master) git status
On branch master
Your branch is up-to-date with 'origin/master'.
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   guide.md
	modified:   images/02.png

no changes added to commit (use "git add" and/or "git commit -a")
➜  git-annex git:(master) ✗ git add .
➜  git-annex git:(master) ✗ git commit -m "change img 02 again, update guide"
[master 263b8bb] change img 02 again, update guide
 2 files changed, 2 insertions(+), 9 deletions(-)
 rewrite images/02.png (100%)
➜  git-annex git:(master) git push origin master
(1 of 1 files) 41.26 KB / 41.26 KB
Counting objects: 5, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (5/5), done.
Writing objects: 100% (5/5), 664 bytes | 0 bytes/s, done.
Total 5 (delta 2), reused 0 (delta 0)
To gitlab.com:gitlab-tests/git-annex.git
   9994265..263b8bb  master -> master
➜  git-annex git:(master) git status
On branch master
Your branch is up-to-date with 'origin/master'.
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   guide.md
	modified:   images/02.png

no changes added to commit (use "git add" and/or "git commit -a")
```

Git add and commit changes - check repo size:

```bash 
➜  git-annex git:(master) ✗ git add .
➜  git-annex git:(master) ✗ git commit -m "change img 02 again to look at the repo's size, update guide"
[master cdd3bf4] change img 02 again to look at the repo's size, update guide
 2 files changed, 6 insertions(+), 2 deletions(-)
➜  git-annex git:(master) git push origin master
(1 of 1 files) 31.44 KB / 31.44 KB
Counting objects: 5, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (5/5), done.
Writing objects: 100% (5/5), 668 bytes | 0 bytes/s, done.
Total 5 (delta 2), reused 0 (delta 0)
To gitlab.com:gitlab-tests/git-annex.git
   263b8bb..cdd3bf4  master -> master
➜  git-annex git:(master) git add .
➜  git-annex git:(master) ✗ git commit -m "update guide"
[master e55054c] update guide
 1 file changed, 4 insertions(+), 1 deletion(-)
➜  git-annex git:(master) git push origin master
Counting objects: 3, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 404 bytes | 0 bytes/s, done.
Total 3 (delta 2), reused 0 (delta 0)
To gitlab.com:gitlab-tests/git-annex.git
   cdd3bf4..e55054c  master -> master
➜  git-annex git:(master) git add .
➜  git-annex git:(master) ✗ git commit -m "modify img 02 again to check repo's size"
[master c2989d3] modify img 02 again to check repo's size
 1 file changed, 2 insertions(+), 2 deletions(-)
➜  git-annex git:(master) git push origin master
(1 of 1 files) 20.20 KB / 20.20 KB
Counting objects: 4, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (4/4), done.
Writing objects: 100% (4/4), 465 bytes | 0 bytes/s, done.
Total 4 (delta 1), reused 0 (delta 0)
To gitlab.com:gitlab-tests/git-annex.git
   e55054c..c2989d3  master -> master
➜  git-annex git:(master) git status
On branch master
Your branch is up-to-date with 'origin/master'.
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   guide.md

no changes added to commit (use "git add" and/or "git commit -a")
➜  git-annex git:(master) ✗ git add .
➜  git-annex git:(master) ✗ git commit -m "update guide"
[master b564909] update guide
 1 file changed, 2 insertions(+), 3 deletions(-)
➜  git-annex git:(master) git push origin master
Counting objects: 3, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 315 bytes | 0 bytes/s, done.
Total 3 (delta 2), reused 0 (delta 0)
To gitlab.com:gitlab-tests/git-annex.git
   c2989d3..b564909  master -> master
➜  git-annex git:(master)
```
