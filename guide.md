# Migrating from Git Annex to Git LFS

Both [Git Annex][] (git-annex) and [Git LFS] are tools to manage large files in Git.

GitLab EE is deprecating [support to git-annex][post-intro-annex] from version 8.17 on (released on 2017/02/22). It will be removed completely by GitLab 9.0 (2017/03/22).

## Configurations

To use [git-annex in GitLab EE][annex-ee], you had first to install and enable it on your server and in your [local environment][install-annex-local]. On GitLab.com, git-annex was enabled, and you had only to install it locally.

### Enabling Annex

Considering that you have git-annex up and running in both remote and local copies, to enable git-annex to your project, you had to:

- Initiate git-annex in your repository

    ```bash
    $ git annex init
    init ok
    (recording state in git...)
    ```

- Track the large files, for example, an `images` directory:

    ```bash
    $ git annex add images/*
    add images/01.png ok
    add images/02.png ok
    (recording state in git...)
    ```

By doing so, git-annex would record the tracked files in the `.git/config` file in your repository root.

Before initiating git-annex, your `.git/config` would look like:

```
[core]
	...
[remote "origin"]
	url = git@gitlab.com:gitlab-tests/git-annex-to-git-lfs.git
	fetch = +refs/heads/*:refs/remotes/origin/*
[branch "master"]
	remote = origin
	merge = refs/heads/master
```

The remote origin for the example above is the test project [git-annex to Git LFS][test-project].

Once you initiate git-annex with `git annex init`, your `.git/config` will look like:

```
[core]
	...
[remote "origin"]
	url = git@gitlab.com:gitlab-tests/git-annex-to-git-lfs.git
	fetch = +refs/heads/*:refs/remotes/origin/*
	annex-uuid = 0455e211-1b0d-41d6-ae84-6f32e9aa7dd8
[branch "master"]
	remote = origin
	merge = refs/heads/master
[annex]
	uuid = 2a91db8f-66ea-491c-8933-6c6fe770bafa
	version = 5
```

You will also find a new directory at `.git/annex/`.

The files you assign to be tracked with git-annex will not affect `.git/config` records. The files are turned into symbolic links that point to data in `.git/annex/objects/`.

The image file will contain the symbolic link, like this:

```
../.git/annex/objects/ZW/1k/SHA256E-s82701--6384039733b5035b559efd5a2e25a493ab6e09aabfd5162cc03f6f0ec238429d.png/SHA256E-s82701--6384039733b5035b559efd5a2e25a493ab6e09aabfd5162cc03f6f0ec238429d.png
```

Once you push to the remote repo, the PNG files (in this case) are not yet at the origin, only symbolic links that point to them. Git-annex knows they're on the local machine, but that's the only place they exist so far. You can ask git-annex where a certain file exists:

```
git annex whereis images/01.png
whereis images/01.png (1 copy)
  	2a91db8f-66ea-491c-8933-6c6fe770bafa -- mycomputer.local:~/GitLab/git-annex-to-git-lfs [here]
ok
```

To push code to your repo, you would:

```bash
git add .
git commit -m "add new files"
git annex sync --content
```

Your files will be found in the branch `master`, but you'll notice that there are more branches created by the `sync` command.

Once you modify the files, you add, commit, and sync, and git-annex will do the rest for you.

Use `git annex info` to retrieve the information about that repository.

### Disabling git-annex

Before changing anything, make sure you have a backup of your repository first. There are a couple ways to do that, but you can simply clone it to another local path and push it to GitLab if you want a remote backup as well.

Here you'll find a guide on [how to back up a git-annex repository to an external hard drive][bkp-ext-drive].

To [stop using git-annex][uninit], you need to run:

To disable git-annex, first, we need to [make sure the git-annex mode is `direct`][stackoverflow-1]:

```bash
➜  git-annex git:(master) git annex direct
commit
On branch master
Your branch is up-to-date with 'origin/master'.
nothing to commit, working tree clean
ok
direct images/01.png ok
direct images/02.png ok
direct  ok
```

Then, we run `git annex uninit`:


```bash
➜  git-annex git:(annex/direct/master) git annex uninit
unannex images/01.png ok
unannex images/02.png ok
Deleted branch git-annex (was 186d141).
```

It will unannex every file in the repository, and remove all of git-annex's other data, leaving you with a git repository plus the previously annexed files.

To make these changes to be applied to the remote repo, we sync the content again: 

```bash
➜  git-annex git:(annex/direct/master) git annex sync --content
(merging origin/git-annex into git-annex...)
(recording state in git...)
commit  (recording state in git...)
(recording state in git...)
ok
pull origin
remote: Counting objects: 5, done.
remote: Compressing objects: 100% (4/4), done.
remote: Total 5 (delta 2), reused 0 (delta 0)
Unpacking objects: 100% (5/5), done.
From gitlab.com:gitlab-tests/git-annex
   186d141..ae4d96e  git-annex  -> origin/git-annex
ok
(merging origin/git-annex into git-annex...)
(recording state in git...)
fatal: Cannot force update the current branch.
git-annex: failed to update refs/heads/master
```

We will notice the error above, because Git doesn't understand the direct mode. So we need to make it indirect again:

```bash
➜  git-annex git:(master) git annex indirect
commit  (recording state in git...)

ok
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)
Untracked files:
	images/

nothing added to commit but untracked files present
ok
indirect  ok
ok
➜  git-annex git:(master) ✗ git status
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)
Untracked files:
  (use "git add <file>..." to include in what will be committed)

	images/

nothing added to commit but untracked files present (use "git add" to track)
```

Now we can add, commit, and push to reflect the changes on the remote repo:

```bash
➜  git-annex git:(master) ✗ git add .
➜  git-annex git:(master) ✗ git commit -m "uninit"
[master 1cd371e] uninit
 2 files changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 images/01.png
 create mode 100644 images/02.png
➜  git-annex git:(master) git push origin master
Counting objects: 7, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (7/7), done.
Writing objects: 100% (7/7), 66.74 KiB | 0 bytes/s, done.
Total 7 (delta 2), reused 0 (delta 0)
To gitlab.com:gitlab-tests/git-annex.git
   943444b..1cd371e  master -> master
➜  git-annex git:(master)
```

Now, for keeping your repo clean, let's remove all git-annex remaining data from your repository.

- On GitLab, navigate to your project branches: `https://gitlab-url/namespace/project-name/branches`
- Delete all branches created by git-annex: `git-annex`, and all under `synced/*`.

### Enabling Git LFS

Git LFS is enabled by default on all GitLab products (GitLab CE, GitLab EE, GitLab.com), therefore, you don't need to do anything in the server side.

First, let's make sure you have Git LFS installed locally:

```bash
git lfs help
```

If the terminal doesn't prompt you with a full response on Git LFS commands, [install LFS][install-lfs] first:

```bash
brew install git-lfs
```

[Enable Git LFS][lfs-track] for the group of files you want to track with it. You can track specific files, all files containing the same extension, or an entire directory:

- Per file:

    git lfs track "images/01.png"

- Per extension:

    git lfs track "*.png"

- Per directory:

    git lfs track "images/*"

Example:

```bash
➜  git-annex git:(master) git lfs track images/*
Tracking images/01.png
Tracking images/02.png
➜  git-annex git:(master) ✗
```

Once you do that, run `git status` and you'll see `.gitattributes` added to your repo. It collects all file patterns that you chose to track via LFS.

To see LFS working, look at the project's size (in bites), update one of your files, and push a change to the remote.

- Repo size: 492 KB
- Add, commit, and push
- Repo size: 492 KB

Though the image has 32KB, the repo remains the same size after pushing it again.

[annex-ee]: https://docs.gitlab.com/ee/workflow/git_annex.html
[bkp-ext-drive]: https://www.thomas-krenn.com/en/wiki/Git-annex_Repository_on_an_External_Hard_Drive
[Git Annex]: http://git-annex.branchable.com/
[Git LFS]: https://git-lfs.github.com/
[install-annex-local]: https://git-annex.branchable.com/install/
[install-lfs]: https://git-lfs.github.com/
[lfs-track]: https://about.gitlab.com/2017/01/30/getting-started-with-git-lfs-tutorial/#tracking-files-with-lfs
[post-intro-annex]: https://about.gitlab.com/2015/02/17/gitlab-annex-solves-the-problem-of-versioning-large-binaries-with-git/
[stackoverflow-1]: http://stackoverflow.com/questions/24447047/remove-git-annex-repository-from-file-tree
[test-project]: https://gitlab.com/gitlab-tests/git-annex-to-git-lfs
[uninit]: https://git-annex.branchable.com/git-annex-uninit/
